
package com.example.Registration.Service;

import com.example.Registration.Dto.UserDTO;
import com.example.Registration.Dto.LoginDTO;
import com.example.Registration.payload.response.LoginMessage;

public interface UserService {
    String addUser(UserDTO userDTO);

    LoginMessage loginUser(LoginDTO loginDTO);

//	String addEmployee(Employee employeeDTO);

}